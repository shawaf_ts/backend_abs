const config = require("config")
const mongoose = require("mongoose")
const jwt = require("jsonwebtoken")
const Joi = require("joi")

const buyerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 50
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 255
  }
})

buyerSchema.methods.generateAuthToken = function() {
  const token = jwt.sign(
    {
      _id: this._id,
      name: this.name,
      email: this.email
    },
    config.get("jwtPrivateKey")
  )
  return token
}

const Buyer = mongoose.model("Buyer", buyerSchema)

function validateBuyer(buyer) {
  const schema = {
    name: Joi.string()
      .min(2)
      .max(50)
      .required(),
    email: Joi.string()
      .min(5)
      .max(50)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .max(255)
      .required()
  }
  return Joi.validate(buyer, schema)
}

function validateSignIn(buyer) {
  const schema = {
    email: Joi.string()
      .min(5)
      .max(50)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .max(255)
      .required()
  }
  return Joi.validate(buyer, schema)
}

exports.Buyer = Buyer
exports.validate = validateBuyer
exports.validateSignIn = validateSignIn
