const mongoose = require("mongoose");
const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const dayOfTheWeek = ["1", "2", "3", "4", "5", "6", "7"];
const bookingStatus = ["PENDING", "ACCEPTED", "REJECTED", "NOT AVAILABLE"];

const appointmentSchema = new mongoose.Schema({
  seller: {
    type: new mongoose.Schema({
      name: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 50
      },
      email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
      }
    }),
    required: true
  },
  buyer: {
    type: new mongoose.Schema({
      name: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 50
      },
      email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
      }
    }),
    required: true
  },
  appointmentDate: {
    type: Date,
    required: true
  },
  appointmentStartTime: {
    type: Number,
    min: 0,
    max: 1440,
    required: true
  },
  appointmentEndTime: {
    type: Number,
    min: 0,
    max: 1440,
    required: true
  },
  statusOfTheBooking: {
    type: String,
    enum: bookingStatus,
    required: true
  }
});

const Appointment = mongoose.model("Appointment", appointmentSchema);

function validateAppointment(appointment) {
  const schema = {
    sellerId: Joi.objectId().required(),
    buyerId: Joi.objectId().required(),
    appointmentDate: Joi.date()
      .greater("now")
      .required(),
    appointmentStartTime: Joi.number().max(1440)
    //appointmentEndTime: Joi.number().max(1440)
    //statusOfTheBooking: Joi.string().valid(bookingStatus)
  };
  return Joi.validate(appointment, schema);
}

function validateStatusOfBooking(appointment) {
  return Joi.validate(appointment, {
    statusOfTheBooking: Joi.string().valid(bookingStatus)
  });
}

exports.Appointment = Appointment;
exports.validate = validateAppointment;
exports.bookingStatus = bookingStatus;
exports.validateStatusOfBooking = validateStatusOfBooking;
