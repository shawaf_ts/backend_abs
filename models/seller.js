const config = require("config");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Joi = require("joi");

// Starting from Monday
// 1: Monday, 2: Tuesday
const dayOfTheWeek = ["1", "2", "3", "4", "5", "6", "7"];

const sellerSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 50
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 255
  },
  place: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 255
  },
  workingDays: {
    type: [String],
    enum: dayOfTheWeek,
    required: true
  },
  startTime: {
    type: Number,
    min: 0,
    max: 1440,
    required: true
  },
  endTime: {
    type: Number,
    min: 0,
    max: 1440,
    required: true
  },
  breakStartTime: {
    type: Number,
    min: 0,
    max: 1440,
    required: true
  },
  breakEndTime: {
    type: Number,
    min: 0,
    max: 1440,
    required: true
  },
  duration: {
    type: Number,
    min: 30,
    max: 240,
    required: true
  }
});

sellerSchema.methods.checkWhetherTimeWithinTheWindow = function(
  appointmentStartTime
) {
  if (
    !(
      appointmentStartTime >= this.startTime &&
      appointmentStartTime <= this.endTime - this.duration
    )
  )
    return false;

  if (
    appointmentStartTime < this.breakEndTime &&
    appointmentStartTime > this.breakStartTime - this.duration
  )
    return false;

  return true;
};

sellerSchema.methods.generateAuthToken = function() {
  const token = jwt.sign(
    {
      _id: this._id,
      name: this.name,
      email: this.email
    },
    config.get("jwtPrivateKey")
  );
  return token;
};

const Seller = mongoose.model("Seller", sellerSchema);

function validateSeller(seller) {
  const schema = {
    name: Joi.string()
      .min(2)
      .max(50)
      .required(),
    email: Joi.string()
      .min(5)
      .max(50)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .max(255)
      .required(),
    place: Joi.string()
      .min(3)
      .max(255)
      .required(),
    workingDays: Joi.array().items(
      Joi.string()
        .max(1)
        .valid(dayOfTheWeek)
        .required()
    ),
    endTime: Joi.number()
      .integer()
      .max(1440)
      .required(),
    startTime: Joi.number()
      .integer()
      .max(1440)
      .less(Joi.ref("endTime"))
      .required(),
    breakEndTime: Joi.number()
      .integer()
      .max(1440)
      .required(),
    breakStartTime: Joi.number()
      .integer()
      .max(1440)
      .less(Joi.ref("breakEndTime"))
      .greater(Joi.ref("startTime"))
      .required(),
    duration: Joi.number()
      .integer()
      .max(240)
      .min(30)
      .required()
  };
  return Joi.validate(seller, schema);
}

function validateSignIn(seller) {
  const schema = {
    email: Joi.string()
      .min(5)
      .max(50)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .max(255)
      .required()
  };
  return Joi.validate(seller, schema);
}

exports.Seller = Seller;
exports.validate = validateSeller;
exports.validateSignIn = validateSignIn;
