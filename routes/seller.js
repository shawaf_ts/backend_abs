const bcrypt = require("bcrypt");
const _ = require("lodash");
const express = require("express");
const { Seller, validate, validateSignIn } = require("../models/seller");
const {
  Appointment,
  validate: validateAppointment,
  bookingStatus
} = require("../models/appointment");
const validateObjectId = require("../middlewares/validateObjectId");
const auth = require("../middlewares/auth");
const router = express.Router();

router.post("/signup", async (req, res) => {
  console.log("Signup");
  console.log(req.body);
  const { error } = validate(req.body);
  if (error) {
    console.log("Error");
    console.log(error);
    return res.status(400).send(error.details[0].message);
  }

  let seller = await Seller.findOne({ email: req.body.email });
  if (seller) return res.status(409).send("User already registered");

  seller = new Seller(
    _.pick(req.body, [
      "name",
      "email",
      "password",
      "place",
      "workingDays",
      "startTime",
      "endTime",
      "breakStartTime",
      "breakEndTime",
      "duration"
    ])
  );
  const salt = await bcrypt.genSalt(10);
  console.log("Seller is:", seller);
  seller.password = await bcrypt.hash(seller.password, salt);
  await seller.save();

  const token = seller.generateAuthToken();
  res
    .header("x-auth-token", token)
    .header("access-control-expose-headers", "x-auth-token")
    .send(_.pick(seller, "_id", "name", "email"));
});

router.post("/signin", async (req, res) => {
  console.log("Signin request Recieved, body:", req.body);
  const { error } = validateSignIn(req.body);
  if (error) {
    console.log("Error in Validation", error);
    return res.status(400).send(error.details[0].message);
  }

  let seller = await Seller.findOne({ email: req.body.email });
  if (!seller) return res.status(400).send("Invalid email or password");

  const validPassword = await bcrypt.compare(
    req.body.password,
    seller.password
  );
  if (!validPassword) return res.status(400).send("Invalid email or password.");

  const token = seller.generateAuthToken();
  res.send(token);
});

router.get("/:id", validateObjectId, async (req, res) => {
  console.log("Get Seller Info", req.params);
  const seller = await Seller.findById(req.params.id).select([
    "-__v",
    "-password"
  ]);
  if (!seller)
    return res.status(404).send("Seller with the given ID was not found.");
  res.send(seller);
});

router.get("/", async (req, res) => {
  const sellers = await Seller.find()
    .select(["-__v", "-password"])
    .sort("name");
  res.send(sellers);
});

module.exports = router;
