const bcrypt = require("bcrypt");
const _ = require("lodash");
const express = require("express");
const { Seller, validate, validateSignIn } = require("../models/seller");
const {
  Appointment,
  validate: validateAppointment,
  bookingStatus,
  validateStatusOfBooking
} = require("../models/appointment");
const validateObjectId = require("../middlewares/validateObjectId");
const auth = require("../middlewares/auth");
const router = express.Router();

router.get("/seller_pending", auth, async (req, res) => {
  console.log(req.user);
  const appointments = await Appointment.find({
    "seller._id": req.user._id,
    statusOfTheBooking: "PENDING"
  });

  if (!appointments) {
    res.status(404).send("Appointment with this sellerId not found");
  }
  res.send(appointments);
});

router.get("/seller_accepted", auth, async (req, res) => {
  console.log(req.user);
  const appointments = await Appointment.find({
    "seller._id": req.user._id,
    statusOfTheBooking: "ACCEPTED"
  });

  if (!appointments) {
    res.status(404).send("Appointment with this sellerId not found");
  }
  res.send(appointments);
});

router.put("/:id", auth, async (req, res) => {
  console.log("Inside Put Request");
  const { error } = validateStatusOfBooking(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const appointment = await Appointment.findByIdAndUpdate(
    req.params.id,
    {
      $set: {
        statusOfTheBooking: req.body.statusOfTheBooking
      }
    },
    { new: true }
  );

  if (!appointment) return res.status(404).send("Appointment Not Found");
  console.log("Successful update");
  res.send(appointment);
});

module.exports = router;

// Appointment.find({
//     "seller._id": req.user.sellerId,
//     appointmentDate: {
//       $gte: new Date(req.body.appointmentDate).setHours(00, 00, 00),
//       $lt: new Date(req.body.appointmentDate).setHours(23, 59, 59)
//     }
//   })
