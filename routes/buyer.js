const bcrypt = require("bcrypt");
const _ = require("lodash");
const jwt = require("jsonwebtoken");
const config = require("config");
const express = require("express");
const { Buyer, validate, validateSignIn } = require("../models/buyer");
const { Seller } = require("../models/seller");
const {
  initSlotArray,
  populateSlotArray,
  validateSlotInfo
} = require("../helpers/slotArray");
const {
  Appointment,
  validate: validateAppointment,
  bookingStatus
} = require("../models/appointment");
const validateObjectId = require("../middlewares/validateObjectId");
const auth = require("../middlewares/auth");
const router = express.Router();

router.post("/signup", async (req, res) => {
  const { error } = validate(req.body);
  if (error) {
    console.log("Error in Validation", error);
    return res.status(400).send(error.details[0].message);
  }

  let buyer = await Buyer.findOne({ email: req.body.email });
  if (buyer) return res.status(400).send("User already registered");

  buyer = new Buyer(_.pick(req.body, ["name", "email", "password"]));
  const salt = await bcrypt.genSalt(10);
  buyer.password = await bcrypt.hash(buyer.password, salt);
  await buyer.save();

  const token = buyer.generateAuthToken();
  res
    .header("x-auth-token", token)
    .header("access-control-expose-headers", "x-auth-token")
    .send(_.pick(buyer, "_id", "name", "email"));
});

router.post("/signin", async (req, res) => {
  console.log("Signin request Recieved, body:", req.body);
  const { error } = validateSignIn(req.body);
  if (error) {
    console.log("Error in Validation", error);
    return res.status(400).send(error.details[0].message);
  }

  let buyer = await Buyer.findOne({ email: req.body.email });
  if (!buyer) return res.status(400).send("Invalid email or password");

  const validPassword = await bcrypt.compare(req.body.password, buyer.password);
  if (!validPassword) return res.status(400).send("Invalid email or password.");

  const token = buyer.generateAuthToken();
  res.send(token);
});

router.post("/signinjwt", async (req, res) => {
  console.log("SigninJwt request Recieved, body:", req.body);
  if (!req.body.token) {
    return res.status(400).send("No Token Provided");
  }
  try {
    jwt.verify(req.body.token, config.get("jwtPrivateKey"));
    res.status(200).send("Signed in");
  } catch (error) {
    return res.status(400).send("Invalid Token");
  }
});

router.get("/", async (req, res) => {
  const buyers = await Buyer.find()
    .select("-__v -password")
    .sort("name");
  res.send(buyers);
});

router.get("/get_appointments", async (req, res) => {
  console.log(req.body.appointmentDate);
  const appointments = await Appointment.find({
    "seller._id": req.body.sellerId,
    appointmentDate: {
      $gte: new Date(req.body.appointmentDate).setHours(00, 00, 00),
      $lt: new Date(req.body.appointmentDate).setHours(23, 59, 59)
    }
  });

  if (!appointments) {
    res.status(404).send("Appointment with the sellerId not found");
  }
  res.send(appointments);
});

router.get("/get_appointments/:id", validateObjectId, async (req, res) => {
  console.log(req.params.id);
  const appointments = await Appointment.find({
    "buyer._id": req.params.id
  }).sort({ appointmentDate: -1 });

  if (!appointments) {
    res.status(404).send("No appointments found");
  }
  res.send(appointments);
});

router.get("/get_appointments/:id", async (req, res) => {
  console.log(req.body.appointmentDate);
  const appointments = await Appointment.find({
    "seller._id": req.body.sellerId,
    appointmentDate: {
      $gte: new Date(req.body.appointmentDate).setHours(00, 00, 00),
      $lt: new Date(req.body.appointmentDate).setHours(23, 59, 59)
    }
  });

  if (!appointments) {
    res.status(404).send("Appointment with the sellerId not found");
  }
  res.send(appointments);
});

router.get("/get_slot_info/:sellerId/:appointmentDate", async (req, res) => {
  // const { error } = validateSlotInfo(req.body);
  const { error } = validateSlotInfo({
    sellerId: req.params.sellerId,
    appointmentDate: req.params.appointmentDate
  });

  if (error) {
    console.log("Error in Validation", error);
    return res.status(400).send(error.details[0].message);
  }

  const seller = await Seller.findById(req.params.sellerId).select(
    "startTime endTime breakStartTime breakEndTime duration"
  );
  if (!seller) return res.status(400).send("Invalid Seller");

  let slotArray = initSlotArray(seller);
  const appointments = await Appointment.find({
    "seller._id": req.params.sellerId,
    appointmentDate: {
      $gte: new Date(req.params.appointmentDate).setHours(00, 00, 00),
      $lte: new Date(req.params.appointmentDate).setHours(23, 59, 59)
    }
  }).sort({ appointmentStartTime: "asc" });

  slotArray = populateSlotArray(appointments, slotArray);

  res.send(slotArray);
});

router.post("/make_appointment", auth, async (req, res) => {
  const { error } = validateAppointment(req.body);
  if (error) {
    console.log("Error in Validation", error);
    return res.status(400).send(error.details[0].message);
  }

  const buyer = await Buyer.findById(req.body.buyerId);
  if (!buyer) return res.status(400).send("Invalid Buyer");

  const seller = await Seller.findById(req.body.sellerId);
  if (!seller) return res.status(400).send("Invalid Seller");

  //TODO: check if the slot of current date is available here

  //check if the time is within sellers speified time
  //and also check for breaks
  if (!seller.checkWhetherTimeWithinTheWindow(req.body.appointmentStartTime))
    return res
      .status(400)
      .send("The time slot is not within the available window");

  let appointment = new Appointment({
    seller: {
      _id: seller._id,
      name: seller.name,
      email: seller.email
    },
    buyer: {
      _id: buyer._id,
      name: buyer.name,
      email: buyer.email
    },
    appointmentDate: new Date(req.body.appointmentDate).setHours(12, 00, 00),
    appointmentStartTime: req.body.appointmentStartTime,
    appointmentEndTime: Number(req.body.appointmentStartTime) + seller.duration,
    statusOfTheBooking: bookingStatus[0]
  });
  try {
    await appointment.save();
  } catch (error) {
    console.log(error);
    res.status(500).send("Something failed.");
  }
  console.log(appointment);
  res.send(appointment);
});
module.exports = router;
