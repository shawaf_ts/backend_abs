# Appointment Booking System Backend API

## 1. General Information  
This API is developed using Node.js Express framework. The API serves as backend API for the Appointment Booking App and Web App. This provides APIs related to creation and modification of Sellers, Buyers, Appointments data.

## 2. Features
API provides endpoints for creating users (both sellers and buyers), appointments.

### 2.1 Authorization
The app offers two types of users: Sellers and Buyers. The API provides endpoint for login and registration for the users.  


### 2.2 Sellers
Sellers refers to bussinesses registered. Sellers interact with this api through Appointment Booking Web App. Sellers can specifcy the name of the bussiness, place, working hours, working days, duration of a typical appointment.  


### 2.3 Buyers
Buyers refers to customers (laymen). Customers can choose sellers and book appointments through the Mobile App.


### 2.4 Appointments
Appointments are the data related to each appointments. Sellers can accept or reject appointments.