const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);
const _ = require("lodash");

exports.validateSlotInfo = data => {
  const schema = {
    sellerId: Joi.objectId().required(),
    appointmentDate: Joi.date()
      .greater("now")
      .required()
  };
  return Joi.validate(data, schema);
};

exports.initSlotArray = ({
  startTime,
  endTime,
  breakStartTime,
  breakEndTime,
  duration
}) => {
  let slotArray = [];
  let stop = (breakStartTime - startTime) / duration;
  for (let i = 0; i < stop; i++) {
    slotArray.push({
      appointmentStartTime: duration * i + startTime,
      appointmentEndTime: duration * i + startTime + duration,
      statusOfTheBooking: "AVAILABLE"
    });
  }

  stop = (endTime - breakEndTime) / duration;
  for (let i = 0; i < stop; i++) {
    slotArray.push({
      appointmentStartTime: duration * i + breakEndTime,
      appointmentEndTime: duration * i + breakEndTime + duration,
      statusOfTheBooking: "AVAILABLE"
    });
  }
  //console.log(slotArray);
  return slotArray;
};

exports.populateSlotArray = (appointments, slotArray) => {
  let bookedSlotArray = [];
  if (appointments) {
    appointments.forEach(element => {
      bookedSlotArray.push(
        _.pick(element, [
          "appointmentStartTime",
          "appointmentEndTime",
          "statusOfTheBooking"
        ])
      );
    });
  }
  //console.log(bookedSlotArray);
  bookedSlotArray.forEach(element => {
    let index = slotArray
      .map(x => x.appointmentStartTime)
      .indexOf(element.appointmentStartTime);

    //console.log("Position:", index);
    slotArray[index] = element;
  });
  //console.log(slotArray);
  return slotArray;
};
