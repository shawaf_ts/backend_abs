const express = require("express");
const config = require("config");
const app = express();
const buyer = require("./routes/buyer");
const seller = require("./routes/seller");
const appointment = require("./routes/appointments");

require("./startup/cors")(app);
require("./startup/db")();

app.use(express.json());
app.use("/api/buyers", buyer);
app.use("/api/sellers", seller);
app.use("/api/appointments", appointment);
const port = process.env.PORT || config.get("port");
const server = app.listen(port, () =>
  console.log(`Listening on port ${port}...`)
);
